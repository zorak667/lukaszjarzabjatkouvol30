﻿using UnityEngine;
using System.Collections;

public class S_ShroomerSenses : S_Base {
    [SerializeField]
    private S_Shroomer shroomer;


    private void OnTriggerEnter2D(Collider2D other) {
        S_Shroom possibleShroom = other.GetComponent<S_Shroom>();
        if (possibleShroom) {
            shroomer.ShroomEnteredSight(possibleShroom);
        }
    }
	
}
