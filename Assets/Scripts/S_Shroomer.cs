﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class S_Shroomer : S_Base {
    private static List<S_Shroomer> instances = new List<S_Shroomer>();
    private static S_Shroomer currentBestShroomer;

    [SerializeField]
    private float viewRange = 1.0f;
    [SerializeField]
    private float speed = 1.0f;
    [SerializeField]
    private CircleCollider2D shroomerSightCollider;
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    private int shroomCount = 0;
    private Vector3 currentDesiredPosition;
    private Vector3 currentDirection;

    private S_Shroom desiredShroom;
    private S_ShroomHouse desiredHouse;

    private void Awake() {
        if (!instances.Contains(this)) {
            instances.Add(this);
        }
    }

    private void Start() {
        CreateNewWanderPosition();
    }

    private void OnValidate() {
        if (shroomerSightCollider) {
            shroomerSightCollider.radius = viewRange;
        }
    }

    private void OnDisable() {
        desiredShroom = null;
    }

    public void IncreaseShroomCount() {
        if (!currentBestShroomer) {
            currentBestShroomer = this;
            OnBecameBestShroomer();
        }
        else {
            shroomCount++;
            if(currentBestShroomer.shroomCount <= shroomCount) {
                currentBestShroomer.OnNoLongerBestShroomer();
                currentBestShroomer = this;
                currentBestShroomer.OnBecameBestShroomer();
            }
        }
    }

    private void Update() {
        ApplyMovement();
    }

    private void ApplyMovement() {
        if(Vector2.Distance(transform.position,currentDesiredPosition) > 0.1f) {
            transform.position += speed * currentDirection * Time.deltaTime;
        }
        else {
            CreateNewWanderPosition();
        }
    }

    private void CreateNewWanderPosition() {
        if (desiredHouse) {
            currentDesiredPosition = desiredHouse.transform.position;
        }
        else if (desiredShroom) {
            currentDesiredPosition = desiredShroom.transform.position;
        }
        else {
            currentDesiredPosition = S_ShroomManager.GetRandomPosition();
        }
        currentDirection = (currentDesiredPosition - transform.position).normalized;
    }
    
    public void ShroomEnteredSight(S_Shroom shroom) {
        if (!desiredShroom) {
            desiredShroom = shroom;
            CreateNewWanderPosition();
        }
    }

    public void EnterHouse(S_ShroomHouse shroomHouse) {
        gameObject.SetActive(false);
        desiredShroom = null;
        desiredHouse = null;
    }

    public void LeaveHouse(S_ShroomHouse shroomHouse) {
        gameObject.SetActive(true);
        CreateNewWanderPosition();
    }

    private void OnBecameBestShroomer() {
        spriteRenderer.color = Color.blue;
    }

    private void OnNoLongerBestShroomer() {
        spriteRenderer.color = Color.white;
    }

    public static void GoHomeAll() {
        foreach(S_Shroomer shroomer in instances) {
            shroomer.GoHome();
        }
    }

    private void GoHome() {
        desiredHouse = S_ShroomHouse.GetClosestVacantHouse(this);
        CreateNewWanderPosition();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        S_Shroom possibleShroom = other.GetComponent<S_Shroom>();
        if (possibleShroom && possibleShroom == desiredShroom) {
            possibleShroom.PickUp(this);
            desiredShroom = null;
            CreateNewWanderPosition();
        }
        else if(desiredHouse){
            S_ShroomHouse possibleHouse = other.GetComponent<S_ShroomHouse>();
            if (possibleHouse == desiredHouse) {
                if (possibleHouse && possibleHouse.Vacant) {
                    possibleHouse.ShroomerEntered(this);
                }
                else {
                    GoHome();
                }
            }
        }
    }


}
