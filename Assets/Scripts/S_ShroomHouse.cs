﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class S_ShroomHouse : S_Base {
    public static List<S_ShroomHouse> instances = new List<S_ShroomHouse>();

    [SerializeField]
    private int numberOfShroomersAllowed;
    private List<S_Shroomer> shroomersInside;


    private void Awake() {
        shroomersInside = new List<S_Shroomer>();
        if (!instances.Contains(this)) {
            instances.Add(this);
        }
    }



    public void ShroomerEntered(S_Shroomer shroomer) {
        if(shroomersInside.Count < numberOfShroomersAllowed && !shroomersInside.Contains(shroomer)) {
            shroomersInside.Add(shroomer);
            shroomer.EnterHouse(this);
        }
    }

    public void ForceShroomersOut() {
        foreach(S_Shroomer shroomer in shroomersInside) {
            shroomer.LeaveHouse(this);
        }
        shroomersInside.Clear();
    }

    public static void ForceShroomersOutAll() {
        foreach(S_ShroomHouse sh in instances) {
            sh.ForceShroomersOut();
        }
    }

    public static S_ShroomHouse GetClosestVacantHouse(S_Shroomer shroomer) {
        S_ShroomHouse retVal = null;
        float minDist = 667.0f;
        float curDist = 0.0f;
        foreach (S_ShroomHouse sh in instances) {
            if (sh.Vacant) {
                curDist = Vector2.Distance(shroomer.transform.position, sh.transform.position);
                if(curDist < minDist) {
                    minDist = curDist;
                    retVal = sh;
                }
            }
        }
        return retVal;
    }

    public bool Vacant {
        get {
            return shroomersInside.Count < numberOfShroomersAllowed;
        }
    }


    
	
}
