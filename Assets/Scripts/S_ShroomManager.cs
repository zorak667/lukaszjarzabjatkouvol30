﻿using UnityEngine;
using System.Collections;

public class S_ShroomManager : S_Base {
    private static S_ShroomManager instance;

    [SerializeField]
    private Camera theCamera;
    [SerializeField]
    private GameObject shroomPrefab;
    [SerializeField]
    private GameObject shroomerPrefab;
    [SerializeField]
    private GameObject shroomHousePrefab;
	
    private void Awake() {
        instance = this;
    }

    public static Vector2 GetRandomPosition() {
        Vector3 randomViewportPos = new Vector3(Random.value, Random.value);
        return instance.theCamera.ViewportToWorldPoint(randomViewportPos);
    }

    public void SpawnShroom() {
        GameObject.Instantiate(shroomPrefab, GetRandomPosition(), Quaternion.identity);
    }

    public void SpawnShroomer() {
        GameObject.Instantiate(shroomerPrefab, GetRandomPosition(), Quaternion.identity);
    }

    public void SpawnShroomHouse() {
        GameObject.Instantiate(shroomHousePrefab, GetRandomPosition(), Quaternion.identity);
    }

    public void SendShroomersHome() {
        S_Shroomer.GoHomeAll();
    }

    public void SendShroomersOut() {
        S_ShroomHouse.ForceShroomersOutAll();
    }
}
