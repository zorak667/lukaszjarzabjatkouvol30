﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class S_Shroom : S_Base {
    public static List<S_Shroom> instances = new List<S_Shroom>();
    [SerializeField]
    private float growthSpeed;

    private S_Shroomer myShroomer;
    
    private void Awake() {
        if (!instances.Contains(this)) {
            instances.Add(this);
        }
    }

    private void OnDestroy() {
        if (instances.Contains(this)) {
            instances.Remove(this);
        }
    }

    private void Update() {
        transform.localScale += Vector3.one * growthSpeed * Time.deltaTime;
    }
	
    public void PickUp(S_Shroomer shroomer) {
        shroomer.IncreaseShroomCount();
        Destroy(gameObject);
    }

}
