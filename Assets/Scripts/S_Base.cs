﻿using UnityEngine;
using System.Collections;

public class S_Base : MonoBehaviour {
    protected Transform thisTransform;

    public new Transform transform{
        get {
            if (!thisTransform) {
                thisTransform = gameObject.GetComponent<Transform>();
            }
            return thisTransform;
        }    
    }
	
}
